#!/bin/sh
rm activities/*.csv
rm activities/*.json

rm users/*.csv
rm users/*.json

rm reports/*.csv
rm reports/*.json
rm reports/*.html
rm reports/*.md

rm users.without.project/*.csv
rm users.without.project/*.json