/**
 * Search common users between 2 instances
 * Export the "intersection" to an xlsx file
 * and a Json file
 */
const DEBUG = process.env.DEBUG || false

const GLClient = require("./libs/gl-cli").GLClient
const fs = require("fs")
const url = require('url')

const jsonexport = require('jsonexport');

require('dotenv').config({ path: './.env' })


let instances = [
  new GLClient({
    baseUri: `${process.env.URL}/api/v4`,
    token: process.env.TOKEN
  }) 
]

console.log("🎃 instances", instances)

async function getAllUsers({glClient, perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}

async function getLicenceInfo({glClient}) {
  //console.log("+++++++++++++++++++++++++++++++++++++++")
  //console.log(glClient)
  //console.log("+++++++++++++++++++++++++++++++++++++++")
  return glClient   // return a promise
    .get({path: `/license`})
    .then(response => response.data)
    .catch(error => error)
}


// await is only valid in async function
async function getInstanceUsers({glClient}) {
  if(DEBUG) {
    console.log("------------------------------------------------------------------------------")
    console.log("🐯 getInstanceUsers / glClient", glClient.baseUri)
  }

  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        /* === user information ====
        { id: 1,
          name: 'Administrator',
          username: 'root',
          state: 'active',
          avatar_url:
          'https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon',
          web_url: 'http://gitlab-3.test/root',
          created_at: '2018-12-10T14:58:14.458Z',
          bio: null,
          location: null,
          public_email: '',
          skype: '',
          linkedin: '',
          twitter: '',
          website_url: '',
          organization: null,
          last_sign_in_at: '2018-12-10T15:02:29.652Z',
          confirmed_at: '2018-12-10T14:58:14.168Z',
          last_activity_on: '2018-12-10',
          email: 'admin@example.com',
          theme_id: 1,
          color_scheme_id: 1,
          projects_limit: 100000,
          current_sign_in_at: '2018-12-10T15:02:29.652Z',
          identities: [],
          can_create_group: true,
          can_create_project: true,
          two_factor_enabled: false,
          external: false,
          private_profile: null,
          is_admin: true,
          shared_runners_minutes_limit: null 
        }
        */
        // --- TRACE ---
        if(DEBUG) {
          console.log(
            "🐱 getAllUsers / page:", page, 
            "someUsers", someUsers.map(item => { 
              return `${item.id} ${item.name} ${item.username} ${item.state}`
              //return { id: item.id, name: item.name, username: item.username, state: item.state }
          }))
        }

        users = users.concat(someUsers.map(user => {
          // TODO: get the user role
          return { 
            id: user.id,
            name: user.name,
            username: user.username, 
            email: user.email,
            instance: url.parse(user.web_url).hostname, // url.parse(glClient.baseUri).hostname
            web_url: user.web_url, 
            state: user.state, 
            is_admin: user.is_admin,
            external: user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project,
            processed: false,
            created_at: user.created_at,
            last_sign_in_at: user.last_sign_in_at,
            confirmed_at: user.confirmed_at,
            current_sign_in_at: user.current_sign_in_at
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }

    /* === user information ====
    [ { id: 1,
        username: 'root',
        email: 'admin@example.com',
        instance: 'gitlab-3.test',
        web_url: 'http://gitlab-3.test/root',
        state: 'active',
        is_admin: true,
        external: false,
        can_create_group: true,
        can_create_project: true,
        processed: false,
        created_at: '2018-12-10T14:58:14.458Z',
        last_sign_in_at: '2018-12-10T15:02:29.652Z',
        confirmed_at: '2018-12-10T14:58:14.168Z',
        current_sign_in_at: '2018-12-10T15:02:29.652Z' } ]
    */

    // --- TRACE ---
    if(DEBUG) {
      console.log("🐸 getInstanceUsers / users", users.map(item => { 
        return `${item.id} ${item.name} ${item.username} ${item.state} isAdmin: ${item.is_admin}`
        //return { id: item.id, name: item.name, username: item.username, state: item.state }
      }))
      console.log("------------------------------------------------------------------------------")
    }

    return users
  } catch (error) {
    console.log("😡:", error)
  }
}


// url.parse(gitLabClient01.baseUri).hostname
async function batch(instances) {
  let allUsers = []
  for(var i in instances) {
    
    /*
    { starts_at: '2018-02-26',
      expires_at: '2020-02-26',
      licensee:
      { Name: 'Philippe Charrière',
        Email: 'pcharriere@gitlab.com',
        Company: 'Bots.Garden' },
      add_ons: {},
      user_limit: 20,
      active_users: 17 }
    */


    await getLicenceInfo({glClient:instances[i]}).then(res => {
      if(DEBUG) {
        console.log("-------------- Licence Information -------------------------------------------")
        console.log("instance: ", url.parse(instances[i].baseUri).hostname)
        console.log("starts: ", res.starts_at)
        console.log("expires: ", res.expires_at)
        console.log("user_limit: ", res.user_limit)
        console.log("active_users: ", res.active_users) // without guests?
        console.log("------------------------------------------------------------------------------")
      }

    })

    await getInstanceUsers({glClient:instances[i]}).then(res => {
      //console.log(">>", url.parse(instances[i].baseUri).hostname, res.length)
      allUsers = allUsers.concat(res)
      //console.log("--->>", allUsers.length)
    })

  }
  if(DEBUG) { console.log("👋  Total users for all instances:", allUsers.length) }

  return allUsers
}

batch(instances).then(allUsers => {
  if(DEBUG) { console.log("👋  (Control) Total users for all instances:", allUsers.length) }
  console.time("Export")

  let usersInOnlyOneInstance = []
  let usersInSeveralInstances = []
  let remaining = []

  allUsers.filter(user=>user.state!=="blocked").forEach(user => {

    //let search = allUsers.filter(item => ((item.email == user.email) && (!user.processed)))  // test username too?
    let search = allUsers.filter(item => item.email == user.email && !user.processed)

    // ⚠️ test status
    //console.log(search)

    /*
      The user exists only one time (in only one instance)
    */
    if(search.length==1) {
      // put the user in the "only one instance" list 
      usersInOnlyOneInstance.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })
      user.processed = true
    } 

    /*
      The user exists several times (in two or more instances)
    */
    if(search.length>1) {
      search.forEach(element => {
        //allUsers.find(item => element.email == item.email && element.instance == item.instance).processed = true
        element.processed = true
      });
       // put the user in the "several instances" list 
      let record = { 
        id: user.id,
        username: user.username, 
        email: user.email,
        instance: search.map(item => item.instance).join("|"),
        state: search.map(item => item.state).join("|")
      }
      // the user exist several time
      usersInSeveralInstances.push(record)
    }


    /*
      The I cannot find the user because it has been marked as processed
      I save it in a list: "remaining" list then I can control at the end
      example:
      if bob exists in G1, G2 and G3
        => bob will be present one time in the "several instances" list
        => bob will be present two times in "remaining" list

        => then the total number of users is 3 = nb users of "several instances" + nb users of "remaining"
    */    
    if(search.length==0) {
      // the user has been already processed
      remaining.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })      
      user.processed = true
    }  
    
  });

  
  fs.writeFileSync(`./users/remaining.json`, JSON.stringify(remaining))

  jsonexport(remaining, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users/remaining.csv`, csv)
  })
  
  fs.writeFileSync(`./users/users-in-only-one-instance.json`, JSON.stringify(usersInOnlyOneInstance))

  jsonexport(usersInOnlyOneInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users/users-in-only-one-instance.csv`, csv)
  })

  fs.writeFileSync(`./users/users-in-several-instances.json`, JSON.stringify(usersInSeveralInstances))

  jsonexport(usersInSeveralInstances, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users/users-in-several-instances.csv`, csv)
  })


  fs.writeFileSync(`./users/all-users.json`, JSON.stringify(allUsers))

  jsonexport(allUsers, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users/all-users.csv`, csv)
  })

  let control = remaining.length+usersInSeveralInstances.length+usersInOnlyOneInstance.length
  let blocked = allUsers.length - control 

  console.log()
  console.log("==================== REPORT ==================================================")
  console.log(" > Total users in all instances             :", allUsers.length, "active inactive and blocked")
  console.log("------------------------------------------------------------------------------")
  console.log(" > Total users in only one instance         :", usersInOnlyOneInstance.length, "users")
  console.log(" > Total users in several instances         :", usersInSeveralInstances.length, "(main admins included)")
  console.log(" > Total remaining users (- blocked)        :", remaining.length)
  console.log("------------------------------------------------------------------------------")
  console.log(" > Control (active + inactive)              :", control)
  console.log(" > Blocked users                            :", blocked)
  console.log("------------------------------------------------------------------------------")
  console.log(" >   Main administrators                    :", instances.length, "(one per instance)")
  console.log(" > + Users in several instances (- 1 admin) :", usersInSeveralInstances.length - 1)
  console.log(" > + Users in one instances                 :", usersInOnlyOneInstance.length)
  //console.log(" > - Blocked users                          :", blocked)
  console.log(" > ______________________________________   : _________")
  console.log(" >   TOTAL (paid users)                     :", usersInSeveralInstances.length + (instances.length - 1) + usersInOnlyOneInstance.length)
  console.log(" >   ⚠️  Guests users are included here")
  console.log("==============================================================================")
  console.timeEnd("Export")

})

