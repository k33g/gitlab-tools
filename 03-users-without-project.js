
const GLClient = require("./libs/gl-cli").GLClient
const fs = require("fs")
const url = require('url')

const jsonexport = require('jsonexport');

require('dotenv').config({ path: './.env' })

let instances = [
  new GLClient({
    baseUri: `${process.env.URL}/api/v4`,
    token: process.env.TOKEN
  }) 
]

async function getAllUsers({glClient, perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}&without_projects=true`})
    .then(response => response.data)
    .catch(error => error)
}

async function getLicenceInfo({glClient}) {
  return glClient   // return a promise
    .get({path: `/license`})
    .then(response => response.data)
    .catch(error => error)
}


// await is only valid in async function
async function getInstanceUsers({glClient}) {

  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {

        users = users.concat(someUsers.map(user => {
          // TODO: get the user role
          return { 
            id: user.id,
            name: user.name,
            username: user.username, 
            email: user.email,
            instance: url.parse(user.web_url).hostname, // url.parse(glClient.baseUri).hostname
            web_url: user.web_url, 
            state: user.state, 
            is_admin: user.is_admin,
            external: user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project,
            processed: false,
            created_at: user.created_at,
            last_sign_in_at: user.last_sign_in_at,
            confirmed_at: user.confirmed_at,
            current_sign_in_at: user.current_sign_in_at
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}


// url.parse(gitLabClient01.baseUri).hostname
async function batch(instances) {
  let allUsers = []
  for(var i in instances) {
    
    await getInstanceUsers({glClient:instances[i]}).then(res => {
      allUsers = allUsers.concat(res)
    })

  }

  return allUsers
}

batch(instances).then(allUsers => {

  let usersInOnlyOneInstance = []
  let usersInSeveralInstances = []
  let remaining = []

  allUsers.filter(user=>user.state!=="blocked").forEach(user => {

    //let search = allUsers.filter(item => ((item.email == user.email) && (!user.processed)))  // test username too?
    let search = allUsers.filter(item => item.email == user.email && !user.processed)

    if(search.length==1) {
      // put the user in the "only one instance" list 
      usersInOnlyOneInstance.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })
      user.processed = true
    } 

    /*
      The user exists several times (in two or more instances)
    */
    if(search.length>1) {
      search.forEach(element => {
        //allUsers.find(item => element.email == item.email && element.instance == item.instance).processed = true
        element.processed = true
      });
       // put the user in the "several instances" list 
      let record = { 
        id: user.id,
        username: user.username, 
        email: user.email,
        instance: search.map(item => item.instance).join("|"),
        state: search.map(item => item.state).join("|")
      }
      // the user exist several time
      usersInSeveralInstances.push(record)
    }

    /*
      The I cannot find the user because it has been marked as processed
      I save it in a list: "remaining" list then I can control at the end
      example:
      if bob exists in G1, G2 and G3
        => bob will be present one time in the "several instances" list
        => bob will be present two times in "remaining" list

        => then the total number of users is 3 = nb users of "several instances" + nb users of "remaining"
    */    
    if(search.length==0) {
      // the user has been already processed
      remaining.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })      
      user.processed = true
    }  
    
  });

  fs.writeFileSync(`./users.without.project/remaining.json`, JSON.stringify(remaining))

  jsonexport(remaining, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users.without.project/remaining.csv`, csv)
  })
  
  fs.writeFileSync(`./users.without.project/users-in-only-one-instance.json`, JSON.stringify(usersInOnlyOneInstance))

  jsonexport(usersInOnlyOneInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users.without.project/users-in-only-one-instance.csv`, csv)
  })

  fs.writeFileSync(`./users.without.project/users-in-several-instances.json`, JSON.stringify(usersInSeveralInstances))

  jsonexport(usersInSeveralInstances, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users.without.project/users-in-several-instances.csv`, csv)
  })


  fs.writeFileSync(`./users.without.project/all-users.json`, JSON.stringify(allUsers))

  jsonexport(allUsers, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./users.without.project/all-users.csv`, csv)
  })

  let control = remaining.length+usersInSeveralInstances.length+usersInOnlyOneInstance.length
  let blocked = allUsers.length - control 

})

