const GLClient = require("./libs/gl-cli").GLClient
require('dotenv').config({ path: './.env' })

let gitLabClient = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})

const faker = require('faker');

async function createUser(gitLabConnection, {username, name, email, password}) {
  console.log({username, name, email, password})
  return gitLabConnection.createUser({username, name, email, password})
}

async function batch() {
  // generate distinct users in the instance
  try {
    for (i = 0; i <= 20; i++) { 
      let email = faker.internet.email()
      let username = name = email.substring(0, email.lastIndexOf("@"))
      let password = "ilovepandas"
      // await is only valid in async function
      console.log("username", username)
      await createUser(gitLabClient, {username, name, email, password})
    }
  } catch (error) {
    console.log("😡:", error)
  }
}

batch()