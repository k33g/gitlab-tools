# Javascript recipes to use GitLab API

> :construction: WIP

## How to run

- fill `.env` file
- run `01-activiries-users.js` or `02-consolidated-report.js`

> the others scripts are wip experiments

## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.
